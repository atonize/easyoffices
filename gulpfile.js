'use strict';

var gulp = require('gulp');
var rename = require('gulp-rename');
var del = require('del');
var rimraf = require('gulp-rimraf');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var svgmin = require('gulp-svgmin');
var svgSymbols = require('gulp-svg-symbols');
var swig = require('gulp-swig');
var fs = require('graceful-fs');
var connect = require('gulp-connect');
var livereload = require('gulp-livereload');
var openBrowser = require('open');
var prettify = require('gulp-html-prettify');
var plumber = require('gulp-plumber');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
global.notify = require('gulp-notify');


// Filter for index.html
function getHtmlFiles() {
  return fs.readdirSync('./public').filter(function(file) {
    return file != 'index.html' && file.indexOf('.html') != -1;
  });
}

// Error function
function swallowError (error) {
    //If you want details of the error in the console
    console.log(error.toString());
    this.emit('end');
}

// Cleaner
gulp.task('clean', function(cb) {
  return del([
    'public'
    ], cb);
});

// JS build tasks
gulp.task('clean:js', function(cb) {
  return del([
    'public/js/**'
    ], cb);
});

gulp.task('compile:js', function() {
  return gulp.src(['src/js/*.js', '!src/js/init.js'])
    .pipe(plumber())
    .pipe(concat('all.js'))
    .pipe(gulp.dest('public/js'))
    .pipe(rename('all.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('public/js'))
});

gulp.task('js', ['clean:js', 'compile:js'], function() {
  return gulp.src('src/js/init.js')
    .pipe(plumber())
    .pipe(rename('init.min.js'))
    // .pipe(uglify())
    .pipe(gulp.dest('public/js'))
    .pipe(livereload());
});

// Html build tasks
gulp.task('clean:html', function(cb) {
  return del([
    'public/*.html'
    ], cb);
});

gulp.task('compile:html', function() {
  return gulp.src('src/markup/*.html')
    .pipe(plumber())
    .pipe(swig({
      defaults: {
        cache: false
      }
    }))
    .pipe(gulp.dest('public'))
});

gulp.task('prettify', ['clean:html'], function() {
  return gulp.src('src/markup/*.html')
    .pipe(plumber())
    .pipe(swig({
      defaults: {
        cache: false
      }
    }))
    .pipe(gulp.dest('public'))
    .pipe(prettify({indent_char: ' ', indent_size: 2}))
    .pipe(gulp.dest('public'))
});


gulp.task('html', ['clean:html', 'compile:html'], function() {
  return gulp.src('src/markup/index.html')
    .pipe(plumber())
    .pipe(swig({
      data: {
        files: getHtmlFiles()
      },
      defaults: {
        cache: false
      }
    }))
    .pipe(gulp.dest('public'))
    .pipe(livereload());
});

// Icons
gulp.task('clean:icons', function(cb) {
  return del([
    'public/icons/**'
  ], cb);
});

gulp.task('icons', ['clean:icons'], function() {
  return gulp.src('src/icons/*.svg')
    .pipe(plumber())
    .pipe(svgmin())
    .pipe(svgSymbols({
      templates: ['default-svg']
    }))
    .pipe(rimraf())
    .pipe(rename('icons.svg'))
    .pipe(gulp.dest('public/icons'))
    .pipe(livereload());
});

// Font tasks
gulp.task('clean:fonts', function(cb) {
  return del([
    'public/fonts/**'
  ], cb);
});

gulp.task('fonts', ['clean:fonts'], function() {
  return gulp.src('src/fonts/**/*.{ttf,woff,eof,otf,eot,svg}')
    .pipe(plumber())
    .pipe(gulp.dest('public/fonts'))
    .pipe(livereload());
});

// Clean images
gulp.task('clean:images', function(cb) {
  return del([
    'public/images/**'
  ], cb);
});

// Clean filters
gulp.task('clean:filters', function(cb) {
  return del([
    'public/filters/**'
  ], cb);
});

// Clean favicon
gulp.task('clean:favicon', function(cb) {
  return del([
    'public/favicon.ico'
  ], cb);
});

gulp.task('images', ['clean:images'], function () {
  var formatJPG = ['src/images/**/*'];
  return gulp.src(formatJPG)
    .pipe(plumber())
    .pipe(gulp.dest('public/images'))
    .pipe(livereload());
});

gulp.task('copy:png', ['clean:images'], function () {
  var formatPNG = ['src/images/**/*.png'];
  return gulp.src(formatPNG)
  .pipe(plumber())
    .pipe(gulp.dest('public/images'))
    .pipe(livereload());
});

gulp.task('copy:filters', ['clean:filters'], function () {
  var formatSVG = ['src/filters/*.svg'];
  return gulp.src(formatSVG)
    .pipe(plumber())
    .pipe(gulp.dest('public/filters'))
    .pipe(livereload());
});

gulp.task('copy:favicon', ['clean:favicon'], function () {
  var favicon = ['src/favicon.ico'];
  return gulp.src(favicon)
    .pipe(plumber())
    .pipe(gulp.dest('public'))
    .pipe(livereload());
});

//Sass
gulp.task('clean:css', function(cb) {
  return del([
    'public/css/**'
  ], cb);
});

gulp.task('sass', ['clean:css'], function() {
  return gulp.src('src/scss/**/*.scss')
    .pipe(plumber({
      errorHandler: notify.onError({
        title: 'SCSS',
        message: function(err) {
          return 'Error: ' + err.message;
        }
      })
    }))
    .pipe(sass({
      outputStyle: 'expanded'
    }))
    .pipe(autoprefixer({
        browsers: ['last 5 versions', 'ie >= 9']
    }))
    .pipe(gulp.dest('public/css'))
    .pipe(livereload());
});

// Webserver
gulp.task('webserver', ['build'], function() {
  connect.server({
    root: ['public', 'public']
  });

  return openBrowser('http://localhost:8080', 'google-chrome');
});

// Watcher
gulp.task('watch', ['webserver'], function() {
  livereload.listen();
  gulp.watch('src/markup/**/*.html', ['html']);
  gulp.watch('src/scss/**/*.scss', ['sass']);
  gulp.watch('src/js/**/*', ['js']);
  gulp.watch('src/icons/**/*.svg', ['icons']);
  gulp.watch('src/fonts/**/*.{ttf,woff,eof,eot,svg}', ['fonts']);
  gulp.watch('src/images/**/*', ['images']);
  gulp.watch('src/images/**/*', ['copy:png']);
  gulp.watch('src/filters/*', ['copy:filters']);
  gulp.watch('src/*', ['copy:favicon']);
});

// Build task
gulp.task('build', ['js', 'html', 'icons', 'fonts', 'images', 'copy:png', 'copy:filters', 'copy:favicon', 'sass']);

// Default task
gulp.task('default', ['webserver']);