
# Getting Started

Install node.js and node package manager (npm): https://www.npmjs.org/

If you don't have bower and gulp installed globally you'll need to run:

```shell
npm install -g gulp
```

After that, from the project's directory run:

```shell
npm install
```

To build the project, from the project's directory run:
```shell
gulp build
gulp watch          # run build on detected changes
```
