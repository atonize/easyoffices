'use strict';

var $ = window.jQuery;

//ON DOCUMENT READY
$(document).ready(function () {
    //Evento pubsub pattern
    var Evento = function () {
        if (!Evento.instance) {
            this.events = {};
            Evento.instance = this;
        }
        return Evento.instance;
    };

    Evento.prototype = {
        on: function (eventName, callback) {
            this.events[eventName] = this.events[eventName] || [];
            this.events[eventName].push(callback)
        },

        emit: function (eventName, data) {
            if (this.events[eventName]) {
                this.events[eventName].forEach(function (callback) {
                    callback(data);
                })
            }
        }
    };

    //SVG for Everybody (ie9+, ...)
    svg4everybody();
    var DatepickerTimeChecker = function (selectedDate) {
        this.selectedDay = selectedDate;
    };

    //Datepicker module
    DatepickerTimeChecker.prototype = {
        currentDay: new Date(),
        currentHour: new Date().getHours(),
        availableTimeList: document.querySelectorAll('.js-get-hour'),

        checkIfSelectedDateIsToday: function () {
            return this.currentDay.toDateString() === this.selectedDay.toDateString() ? true : false;
        },

        checkPastHours: function () {
            if (this.checkIfSelectedDateIsToday()) {
                this.removePastHours();
            } else {
                this.showAllAvailableHours();
            }
        },

        showAllAvailableHours: function () {
            this.availableTimeList.forEach(function (item) {
                item.classList.remove('hide');
            });
        },

        removePastHours: function () {
            var currentHour = this.currentHour;
            this.availableTimeList.forEach(function (item) {
                if (item.dataset.hour <= currentHour) {
                    item.classList.add('hide');
                }
            });
        },

        calculateMinDate: function () {
            if (this.currentDay.getDay() == 6) {
                return 2;
            } else if (this.currentDay.getDay() == 0) {
                return 1;
            } else {
                return this.currentHour >= 17 ? 1 : 0;
            }
        }
    };

    $("#datepicker").datepicker({
        firstDay: 1,
        beforeShowDay: $.datepicker.noWeekends,
        minDate: null,
        onSelect: function () {
            $(this).datepicker("option", "minDate", new DatepickerTimeChecker().calculateMinDate());
            new DatepickerTimeChecker($(this).datepicker('getDate')).checkPastHours()
        }
    });
    $("#datepicker").datepicker('setDate', '').find('.ui-datepicker-current-day, .ui-state-active').removeClass('ui-datepicker-current-day ui-state-active');

    // forEach function
    var forEach = function (array, callback, scope) {
        for (var i = 0; i < array.length; i++) {
            callback.call(scope, i, array[i]); // passes back stuff we need
        }
    };



    // tiny-slider initialisation
    var sliders = document.querySelectorAll('.slider');
    forEach(sliders, function (index, value) {
        var slider1 = tns({
            container: value,
            items: 1,
            slideBy: 'page',
            autoplay: false,
            mouseDrag: true
        });
    });

    //scroll to section
    $(document).on('click', 'a[href^="#"]', function (e) {
        e.preventDefault();

        var target = this.hash,
            $target = $(target);

        if ($target.length) {

            $('html, body').stop().animate({
                'scrollTop': $target.offset().top - 120
            }, 800, 'swing', function () {
                window.location.hash = target;
            });

        }

    });

    //Search module
    var Search = function () {
        $('.js-search-item-trigger').on('click', this.toggleSearchItem.bind(this));
        $(document).on('click', this.closeSearchItemWhenClickOutside.bind(this));
        $('.js-nav-menu').on('click', this.showHideMobileNav.bind(this));
    };

    Search.prototype = {

        toggleSearchItem: function (e) {
            $('.js-search-item').removeClass('is-active');
            var clickedLink = $(e.currentTarget);
            var clickedLinkParent = clickedLink.parent();
            clickedLinkParent.addClass('is-active');
            $('body').addClass('search-menu-active');
        },

        closeSearchItemWhenClickOutside: function (e) {
            e.stopPropagation();
            if ($(e.target).parents('.js-search-item').length === 0) {
                $('.js-search-item').removeClass('is-active');
                $('body').removeClass('search-menu-active');
            }
        },

        showHideMobileNav: function () {
            $('body').toggleClass('menu-is-active');
        }

    };

    var PropertiesList = function () {
        $('.js-toggle-link').on('click', this.toggleText.bind(this));
        $('.js-expand-properties').on('click', this.expandAllProperties.bind(this));
        $('.js-shrink-properties').on('click', this.shrinkAllProperties.bind(this));
        $('.js-expand-property').on('click', this.showSingleProperty.bind(this));
        $('.js-shrink-property').on('click', this.hideSingleProperty.bind(this));
    };

    PropertiesList.prototype = {

        toggleText: function() {
            $('.js-toggle-link').children().toggleClass('is-active');
        },

        expandAllProperties: function () {
            $('.js-property-list-item').addClass('is-expanded-single');
        },

        shrinkAllProperties: function() {
            $('.js-property-list-item').removeClass('is-expanded-single');
        },

        showSingleProperty: function (e) {
            $('.js-property-list-item').removeClass('is-expanded-single');
            $(e.currentTarget).parents('.js-property-list-item').addClass('is-expanded-single');
        },

        hideSingleProperty: function (e) {
            $(e.currentTarget).parents('.js-property-list-item').removeClass('is-expanded-single');
        }
    };

    var Tabs = function () {
        this.createTabIds();
        $(this.tabLink).on('click', this.onTabClick.bind(this));
    };

    Tabs.prototype = {
        tabLink: '.js-tab-link',
        tabContent: '.js-tab-content',

        createTabIds: function () {
            var allTabLinks = document.querySelectorAll(this.tabLink);
            var allTabContent = document.querySelectorAll(this.tabContent);
            allTabLinks.forEach(function (el, index) {
                el.setAttribute('data-tab', 'tab-' + index);
            });
            allTabContent.forEach(function (el, index) {
                el.setAttribute('id', 'tab-' + index);
            });
        },

        onTabClick: function (event) {
            this.selectedTabId = event.currentTarget.dataset.tab;
            this.showTab();
        },

        showTab: function () {
            var clickedTab = $("[data-tab='" + this.selectedTabId + "']")
            clickedTab.siblings().removeClass('current');
            clickedTab.parents('.js-tab-group').find(this.tabContent).removeClass('current');
            clickedTab.addClass('current');
            $('#' + this.selectedTabId).addClass('current');
        }
    };

    var StepIterator = function () {
        $(this.firstStepSelector).on('click', this.checkIfHaveItems.bind(this));
        $(this.nextStepSelector).on('click', this.onNextStepSubmit.bind(this));
        $(this.prevStepSelector).on('click', this.onPrevStepSubmit.bind(this));
        $('.js-close-shortlist-overlay').on('click', this.resetForm.bind(this));
    };

    StepIterator.prototype = {
        stepIndex: 1,
        firstStepSelector: '.js-open-first-step',
        nextStepSelector: '.js-next-step-submit',
        prevStepSelector: '.js-prev-step-submit',
        stepContainerSelector: '.js-shortlist-step',
        totalSteps: $('.js-shortlist-step').length,

        checkIfHaveItems: function() {
            var checkNumberOfShortlistItems = $('#shortlist').attr('data-total');
            if (checkNumberOfShortlistItems === "0") {
                return false;
            } else {
                this.incrementStepIndex();
                this.hideAllSteps();
                this.showSecondStep();
                this.openShortlistOverlay();
            }
        },

        openShortlistOverlay:function() {
            $('body').addClass('overlay-is-active');
        },

        showFirstStep: function () {
            $('[data-index="1"]').addClass('is-active');
        },

        showSecondStep: function () {
            $('[data-index="2"]').addClass('is-active');
        },

        hideAllSteps: function () {
            $(this.stepContainerSelector).removeClass('is-active');
        },

        incrementStepIndex: function () {
            this.stepIndex++;
        },

        decrementStepIndex: function () {
            this.stepIndex--;
        },

        showStep: function () {
            $('[data-index="' + this.stepIndex + '"]').addClass('is-active');
        },

        hideForm: function () {
            $('.js-shortlist-steps').addClass('hide');
        },

        showLastStepMessage: function () {
            $('.js-shortlist-step-last').addClass('is-active');
            $('html').addClass('is-active-thank-you-page');
        },

        checkStepValidity: function(stepType) {
            switch (stepType) {
                case 'radio':
                    return this.checkIsRadioChecked();
                case 'datepicker':
                    return this.checkIsDateSelected();
                default:
                    return true;
            }
        },

        checkIsDateSelected: function() {
            var checkDatepickerValue = $('#datepicker').datepicker('getDate');
            if (checkDatepickerValue === null) {
                this.addErrorMessage('You must select a date!');
                this.markButtonIfStepIsNotValid();
                return false;
            } else {
                this.removeErrorMessage();
                this.unmarkButtonIfStepIsValid();
                return true;
            }
        },

        checkIsRadioChecked: function() {
            var radios = $('.js-shortlist-step.is-active .input-radio');
            var formValid = false;

            var i = 0;
            while (!formValid && i < radios.length) {
                if (radios[i].checked) {
                    formValid = true;
                } 
                i++;
            }

            if (!formValid) {
                this.addErrorMessage('You must choose some option!');
                this.markButtonIfStepIsNotValid();
            } else {
                this.removeErrorMessage();
                this.unmarkButtonIfStepIsValid();
            }
            return formValid;
        },

        addErrorMessage: function(errorText) {
            $('.js-shortlist-step.is-active').find('.error-message').remove();
            $('.js-shortlist-step.is-active').append('<div class="error-message">' + errorText +'</div>');
        },

        removeErrorMessage: function() {
            $('.js-shortlist-step.is-active').find('.error-message').remove();
        },

        markButtonIfStepIsNotValid: function() {
            $('.js-next-step-submit').addClass('is-not-valid');
        },

        unmarkButtonIfStepIsValid: function() {
            $('.js-next-step-submit').removeClass('is-not-valid');
        },

        getStepType: function() {
            return $('.js-shortlist-step.is-active').attr('data-type');
        },

        onNextStepSubmit: function () {
            if (!this.checkStepValidity(this.getStepType())) {
                return false;
            } 

            this.incrementStepIndex();
            if (this.stepIndex <= this.totalSteps) {
                this.hideAllSteps();
                this.showStep();
            } else {
                this.hideAllSteps();
                this.hideForm();
                this.showLastStepMessage();
            }
        },

        onPrevStepSubmit: function () {
            this.decrementStepIndex();
            this.hideAllSteps();
            this.showStep();
        },

        resetForm: function () {
            $('.js-shortlist-step-last').removeClass('is-active');
            $('body').removeClass('overlay-is-active');
            $('html').removeClass('is-active-thank-you-page');
            $('.js-shortlist-steps').removeClass('hide');
            this.hideAllSteps();
            this.showFirstStep();
            this.stepIndex = 1;
        }

    };

    var ShortlistControler = function () {

        $('.js-add-to-shortlist').on('click', this.tryAddItem.bind(this));
        $(document).on('click', '.js-remove-from-shortlist', this.tryRemoveItem.bind(this));
    };

    ShortlistControler.prototype = {
        maxNumberOfItems: 6,
        numberOfItems: 0,

        tryAddItem: function (e) {
            if (this.numberOfItems < this.maxNumberOfItems) {
                this.addToList(e);
                this.toggleAddRemoveText(e);
            } else {
                alert('Shortlist reached maximum number of items!');
            }
        },

        tryRemoveItem: function (e) {
            if (this.numberOfItems !== 0) {
                //close shortilist overlay if last item is removed
                if (this.numberOfItems === 1) {
                    this.closeShortlistOverlay();
                }
                this.removeFromList(e);
                this.toggleAddRemoveText(e);                
            } else {
                alert('Shortlist is empty!');
            }
        },

        addToList: function (e) {
            this.numberOfItems++;
            this.setTotalCountData();
            new Evento().emit('SHORTLIST|AMOUNT|INCREMENT', { numberOfItems: this.numberOfItems, name: e.currentTarget.dataset.name, image: e.currentTarget.dataset.image, propertyId: e.currentTarget.dataset.propertyId, });
            this.openShortlistOverlay();
        },

        removeFromList: function (e) {
            this.numberOfItems--;
            this.setTotalCountData();
            new Evento().emit('SHORTLIST|AMOUNT|DECREMENT', { numberOfItems: this.numberOfItems, name: e.currentTarget.dataset.name, image: e.currentTarget.dataset.image, propertyId: e.currentTarget.dataset.propertyId, });
        },

        setTotalCountData: function() {
            $('#shortlist').attr('data-total', this.numberOfItems);
        },

        toggleAddRemoveText: function (e) {
            $(e.currentTarget).addClass('hide');
            $(e.currentTarget).siblings().removeClass('hide');
        },

        openShortlistOverlay: function() {
            $('body').addClass('overlay-is-active');
        },

        closeShortlistOverlay: function () {
            $('body').removeClass('overlay-is-active');
        }
    };

    var ShortlistAmount = function () {
        new Evento().on('SHORTLIST|AMOUNT|INCREMENT', this.incrementAmount.bind(this));
        new Evento().on('SHORTLIST|AMOUNT|DECREMENT', this.decrementAmount.bind(this));
    };

    ShortlistAmount.prototype = {

        incrementAmount: function (data) {
            $('.js-short-list-number-of-items').text(data.numberOfItems);
        },

        decrementAmount: function (data) {
            $('.js-short-list-number-of-items').text(data.numberOfItems);
        }
    };

    var ShortlistView = function () {
        new Evento().on('SHORTLIST|AMOUNT|INCREMENT', this.addItemToTheList.bind(this));
        new Evento().on('SHORTLIST|AMOUNT|DECREMENT', this.removeItemFromTheList.bind(this));
    };

    ShortlistView.prototype = {
        allListItems: [],
        maxNumberOfItems: 6,
        numberOfItems: null,

        addItemToTheList: function (data) {
            this.allListItems.push(data);
            this.numberOfItems = data.numberOfItems;
            this.createMarkupForItem();
        },

        removeItemFromTheList: function(data) {
            var itemForRemoveId = data.propertyId;
            var itemForRemoveIndex;

            this.allListItems.forEach(function (item, index) {
                if (item.propertyId === itemForRemoveId) {
                    return itemForRemoveIndex = index;
                }
            });
            this.allListItems.splice(itemForRemoveIndex, 1);
            this.numberOfItems = data.numberOfItems;
            this.createMarkupForItem();
            this.changeButtonState(itemForRemoveId);
        },

        changeButtonState: function(buttonId) {
            var buttonForChangeState = $(".js-remove-from-shortlist[data-property-id='" + buttonId + "']")
            buttonForChangeState.addClass('hide');
            buttonForChangeState.siblings().removeClass('hide');
        },

        createMarkupForItem: function () {
            var itemsMarkup = '';
            var titleMarkup = '';
            this.allListItems.forEach(function(item) {
                itemsMarkup +=
                    '<div class="shortlist-item">' +
                    '<img class="shortlist-img" src="' + item.image + '">' +
                    '<p class="shortlist-property-name">' + item.name + '</p>' +
                    '<a href="javascript:;" class="js-remove-from-shortlist shortlist-remove-btn" data-property-id="' + item.propertyId + '" data-name="' + item.name + '" data-image="' + item.image + '">Remove</a>' +
                    '</div>';

                titleMarkup += '<div class="radio-form-group">' +
                    '<input class="input-radio" type="radio" id="' + item.propertyId + '" name="offices" value="' + item.propertyId + '">' +
                    '<label class="radio-label" for="' + item.propertyId + '" data-property-id="' + item.propertyId + '">' + item.name + '</label>' +
                    '</div>';
            });

            for (var index = 0; index < this.maxNumberOfItems - this.numberOfItems; index++) {
                itemsMarkup +=
                    '<a href="#properties" class="shortlist-item shortlist-item-empty">+</a>'
                titleMarkup += '';
            }

            $('.shortlist-items').html(itemsMarkup);
            $('.js-shortlist-selected-items').html(titleMarkup);

            // if only one office in the shortlist add checked attribute
            if ($('.input-radio[name="offices"]').length === 1) {
                $('.input-radio[name="offices"]').first().prop('checked', true);
            }
        }
    };

    //Run modules
    new Search();
    new PropertiesList();
    new Tabs();
    new StepIterator();
    new ShortlistControler();
    new ShortlistAmount();
    new ShortlistView();
});


//Single maps (showed on every property location tab)
function singleMaps() {
    var map, directionsDisplay, directionsService, currentPosition, lat, lng, mapId;
    var maps = document.querySelectorAll(".maps");
    var mapInstances = [];

    maps.forEach(function (el, index) {
        el.setAttribute('id', 'map-' + index);
        mapId = document.getElementById(el.id);
        lat = mapId.getAttribute('data-lat');
        lng = mapId.getAttribute('data-lng')

        map = new google.maps.Map(mapId, {
            zoom: 17,
            center: new google.maps.LatLng(parseFloat(lat), parseFloat(lng)),
            mapTypeId: 'roadmap',
            fullscreenControl: false,
            streetViewControl: false,
            mapTypeControl: true,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                position: google.maps.ControlPosition.LEFT_TOP
            },
            zoomControlOptions: {
                position: google.maps.ControlPosition.RIGHT_BOTTOM
            }
        });

        //store map instances to use on get directions
        mapInstances.push({ map: map, mapId: 'map-' + index });

        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(parseFloat(lat), parseFloat(lng)),
            map: map,
            icon: 'http://staging.easyoffices.com/assets/rovva/office-summary/images/pin.svg'
        });
    });

    //Get current user position and store data to global currentPosition variable
    currentPosition = {
        lat: 51.432190, // 51.432190, -0.525320
        lng: -0.525320
    }


    /*    if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                currentPosition = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
            }, function () {
                // handleLocationError(true, markerme);
            });
        } else {
    
            window.alert('Geolocation is not supported');
        }*/

    //get directions - (use data from clicked element for inner function for routeCalculation)
    $('.js-get-directions').on('click', function (e) {
        var lat = e.currentTarget.dataset.lat;
        var lng = e.currentTarget.dataset.lng;
        var mapId = e.currentTarget.dataset.map;
        var mapa;
        //get needed map instance (based on data-map="map-id")
        mapInstances.forEach(function (item) {
            if (item.mapId == mapId) {
                mapa = item.map;
            }
        });

        directionsDisplay = new google.maps.DirectionsRenderer;
        directionsService = new google.maps.DirectionsService;
        directionsDisplay.setMap(mapa);
        calculateAndDisplayRoute(directionsService, directionsDisplay, currentPosition, lat, lng);
    });

    //Show street view on the map
    $('.js-street-view-link').on('click', function (e) {
        var lat = e.currentTarget.dataset.lat;
        var lng = e.currentTarget.dataset.lng;
        var mapId = e.currentTarget.dataset.map;
        var mapa;

        //get needed map instance (based on data-map="map-id")
        mapInstances.forEach(function (item) {
            if (item.mapId == mapId) {
                mapa = item.map;
            }
        });

        var panorama = mapa.getStreetView();
        panorama.setPosition({ lat: parseFloat(lat), lng: parseFloat(lng) });
        panorama.setPov({ heading: 265, pitch: 0 });

        var toggle = panorama.getVisible();
        if (toggle == false) {
            panorama.setVisible(true);
        } else {
            panorama.setVisible(false);
        }
    });

    function calculateAndDisplayRoute(directionsService, directionsDisplay, currentPos, latDes, lngDes) {
        var latDest = parseFloat(latDes);
        var lngDest = parseFloat(lngDes);
        var dest = { lat: JSON.parse(latDest), lng: JSON.parse(lngDest) };

        directionsService.route({
            origin: currentPos,  // current position.
            destination: dest,  // destination.
            travelMode: google.maps.TravelMode.DRIVING
        }, function (response, status) {
            if (status == 'OK') {
                directionsDisplay.setDirections(response);
            } else {
                window.alert('Directions request failed due to ' + status);
            }
        });
    }
}

google.maps.event.addDomListener(window, 'load', singleMaps);


// icons
var iconNormal = 'http://staging.easyoffices.com/assets/rovva/office-summary/images/pin1.png';
var iconSelected = 'http://staging.easyoffices.com/assets/rovva/office-summary/images/pin.svg';

var MapAndProperties = function() {
    this.getPropertiesDataFromMarkup();
    this.showOnlyTenProperties();
    this.initMap();
    this.searchByAddress();

    $('.js-open-map-item').on('click', this.openMarker.bind(this));
    $('.js-view-all-properties').on('click', this.showAllProperties.bind(this));
    $("#price-low-to-high").on('click', this.sortPriceLowToHight.bind(this));
    $("#price-high-to-low").on('click', this.sortPriceHighToLow.bind(this));
    $("#price-default").on('click', this.sortPriceDefault.bind(this));
    $("input:checkbox").on('click', this.sortByFacilities.bind(this));
}

MapAndProperties.prototype = {
    map: null,
    markers: [],
    properties: [],
    propertiesData: $(".js-property-data"),
    propertiesDataArray: document.querySelectorAll('.js-property-list-item'),

    showOnlyTenProperties: function() {
        if (this.propertiesData.length > 10) {
            $('.js-view-all-properties-holder').show();
            $('.js-property-list-item').filter(function (itemIndex) {
                return itemIndex > 9;
            }).hide();
        }
    },

    sortByFacilities: function() {
        this.showAllProperties();
        var facilities = [];
        var allProperties = $("[id*=property-]");
        $("input[name='facilities']:checked").each(function () {
            facilities.push($(this).val());
            var findme = $(this).val();
        });
        if (facilities.length == 0) {
            for (var i = 0; i < allProperties.length; i++) {
                allProperties[i].style.display = "block";
            }
        } else {
            for (var i = 0; i < allProperties.length; i++) {
                for (var j = 0; j < facilities.length; j++) {
                    var findme = facilities[j];
                    var a = $(allProperties[i]).find("[data-facility=" + findme + "]");

                    if (a.length == 0) {
                        allProperties[i].style.display = "none";
                        break;
                    } else { 
                        allProperties[i].style.display = "block";
                    }
                }
            }
        }
    },

    sortPriceLowToHight:function() {
        this.showAllProperties();
        $("#toSort .property-list-item ").sort(function (a, b) {
            var contentA = parseInt($(a).data('price'));
            var contentB = parseInt($(b).data('price'));
            return (contentA < contentB) ? -1 : (contentA > contentB) ? 1 : 0;
        }).appendTo("#toSort");
    },

    sortPriceHighToLow: function() {
        this.showAllProperties();
        $("#toSort .property-list-item").sort(function (a, b) {
            var contentA = parseInt($(a).data('price'));
            var contentB = parseInt($(b).data('price'));
            return (contentA > contentB) ? -1 : (contentA < contentB) ? 1 : 0;
        }).appendTo("#toSort");
    },

    sortPriceDefault: function() {
        this.showAllProperties();
        $("#toSort .property-list-item").sort(function (a, b) {
            var contentA = parseInt($(a).data('defaultprice'));
            var contentB = parseInt($(b).data('defaultprice'));
            return (contentA < contentB) ? -1 : (contentA > contentB) ? 1 : 0;
        }).appendTo("#toSort");
    },

    showAllProperties:function() {
        $('.js-view-all-properties-holder').hide();
        $('.js-property-list-item').show();
    },

    runDistanceSlider: function(maxDistance, minDistance) {
        var self = this;
        $("#distance-slider").slider({
            range: true,
            min: minDistance,
            max: maxDistance,
            values: [minDistance, maxDistance],
            step: 0.1,
            slide: function (event, ui) {
                $("#distance").val(ui.values[0] + " - " + ui.values[1] + " miles");
                self.showPropertiesByDistance(ui.values[0], ui.values[1]);
            }
        });
        $("#distance").val($("#distance-slider").slider("values", 0) +
            " - " + $("#distance-slider").slider("values", 1) + " miles");
        this.showAllProperties();
    },

    showPropertiesByDistance: function(minValue, maxValue) {
        $('.js-property-list-item').hide();
        $('.js-property-list-item').filter(function () {
            return $(this).data('distance') <= maxValue && $(this).data('distance') >= minValue;
        }).show();
    },
        
    getPropertiesDataFromMarkup: function () {
        for (var i = 0; i < this.propertiesData.length; i++) {
            var lat = this.propertiesData[i].dataset.lat;
            var lng = this.propertiesData[i].dataset.lng;
            var name = this.propertiesData[i].dataset.name;
            var image = this.propertiesData[i].dataset.image;
            var propertyId = this.propertiesData[i].dataset.propertyId;
            this.properties.push(['<a href="#' + propertyId + '">' +
                '<img src="' + image + '" class="infobox-image" />' +
                '<h3 class="infobox-title">' + name + '</h3></a>', lat, lng]);
        }
    },

    initMap: function () {
        this.map = new google.maps.Map(document.getElementById('map'), {
            zoom: 10,
            styles: [
                {
                    featureType: "all",
                    stylers: [
                        { saturation: -80 },
                        { lightness: 50 }
                    ]
                }
            ],
            fullscreenControl: true,
            streetViewControl: false,
            mapTypeControl: false,
            zoomControlOptions: {
                position: google.maps.ControlPosition.TOP_LEFT
            }
        });
        this.setMarkers(this.map);
    },

    searchByAddress: function() {
        var self = this;
        var input = document.getElementById('input-search');
        var map = this.map;

        var autocomplete = new google.maps.places.Autocomplete(input, {
            types: ["address"],
            componentRestrictions: { country: "uk" }
        });
        autocomplete.bindTo('bounds', map);
        // var infowindow = new google.maps.InfoWindow();

        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            // // infowindow.close();
            var place = autocomplete.getPlace();

            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(10);
            }
            self.moveMarker(place.name, place.geometry.location);
            self.calculateDistanceBetweenAddressAndAllMarkers(place.geometry.location);
        });

    },

    setMarkers: function (map, newMarker) {
        var bounds = new google.maps.LatLngBounds();
        var markers = [];

        if (newMarker === undefined) {
            for (var i = 0; i < this.properties.length; i++) {
                var property = this.properties[i];
                var marker = new google.maps.Marker({
                    position: {
                        lat: parseFloat(property[1]),
                        lng: parseFloat(property[2])
                    },
                    map: this.map,
                    animation: google.maps.Animation.DROP,
                    title: property[0],
                    icon: iconNormal
                });
                bounds.extend(marker.getPosition());

                // set infobox for all markers
                var infowindow = new google.maps.InfoWindow({
                    content: property[0]
                });

                marker.infobox = infowindow;
                markers.push(marker);

                // open marker on click
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        // Close all other infoboxes
                        for (var j = 0; j < markers.length; j++) {
                            markers[j].infobox.close(map);
                            markers[j].setIcon(iconNormal);
                        }
                        // Open correct info box
                        markers[i].infobox.open(map, markers[i]);
                        markers[i].setIcon(iconSelected);
                    }
                })(marker, i));

                //Close infowindow when click outside
                google.maps.event.addListener(map, "click", function (event) {
                    markers.forEach(function (item) {
                        item.infobox.close(map);
                        item.setIcon(iconNormal);
                    });
                });
            }
        } else {
            //allow only one address marker
            if (this.properties.length > this.propertiesData.length) {
                this.properties.pop();
                this.markers[this.propertiesData.length].setMap(null);
            }
            this.properties.push(newMarker);

            for (var i = 0; i < this.properties.length; i++) {
                var property = this.properties[i];

                if (i == this.properties.length - 1) {
                    var marker = new google.maps.Marker({
                        position: property[1],
                        map: this.map,
                        animation: google.maps.Animation.DROP,
                        icon: iconSelected
                    });
                    bounds.extend(marker.getPosition());

                    // set infobox for last marker
                    var infowindow = new google.maps.InfoWindow({
                        content: property[0]
                    });
                    marker.infobox = infowindow;
                    markers.push(marker);
                } else {

                    var marker = new google.maps.Marker({
                        position: {
                            lat: parseFloat(property[1]),
                            lng: parseFloat(property[2])
                        }
                    });
                    bounds.extend(marker.getPosition());
                    // set infobox for all markers
                    var infowindow = new google.maps.InfoWindow({
                        content: property[0]
                    });
                    marker.infobox = infowindow;
                    markers.push(marker);
                }
                // open marker on click
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        // Close all other infoboxes
                        for (var j = 0; j < markers.length - 1; j++) {

                            markers[j].infobox.close(map);
                            markers[j].setIcon(iconNormal);

                        }
                        // Open correct info box
                        markers[i].infobox.open(map, markers[i]);
                        markers[i].setIcon(iconSelected);
                    }
                })(marker, i));

                //Close infowindow when click outside
                google.maps.event.addListener(this.map, "click", function (event) {
                    marker.forEach(function (item) {
                        item.infobox.close(this.map);
                        item.setIcon(iconNormal);
                    });
                });

            }
        }
        this.map.fitBounds(bounds);
        this.markers = markers;
    },

    calculateDistanceBetweenAddressAndAllMarkers: function(selectedAddress) {
        var distancesList = [];

        for (var i = 0; i < this.markers.length - 1; i++) {
            var marker = {
                lat: this.markers[i].position.lat,
                lng: this.markers[i].position.lng
            }
            //get single distance
            var distance = ((google.maps.geometry.spherical.computeDistanceBetween(marker, selectedAddress) / 1609.344).toFixed(1));
            distancesList.push(distance);
            
            //write data on properties list
            this.propertiesDataArray[i].dataset.distance = distance;
            this.propertiesDataArray[i].querySelector('.distance-from-address').textContent =  distance + ' miles';
        }

        //get maximum distance marker
        var maxDistanceMarker = distancesList.reduce(function (a, b) {
            return Math.max(a, b);
        });
        //get minimum distance marker
        var minDistanceMarker = distancesList.reduce(function (a, b) {
            return Math.min(a, b);
        });
        this.runDistanceSlider(maxDistanceMarker, minDistanceMarker);
        this.sortPropertiesByDistance();
    },

    sortPropertiesByDistance: function() {
        $("#toSort .property-list-item").sort(function (a, b) {
            var result;
            a = a.dataset.distance.split('.'),
            b = b.dataset.distance.split('.');

            while (a.length) {
                result = a.shift() - (b.shift() || 0);
                if (result) {
                    return result;
                }
            }
            return -b.length;
        }).appendTo("#toSort");
    },

    moveMarker: function(placeName, latlng) {
        $(".js-search-item-distance").css('pointer-events', 'initial');
        var newEl = [placeName, latlng];
        this.setMarkers(this.map, newEl)
    },

    openMarker: function(event) {
        var targetElement = event.currentTarget;
        var id = targetElement.getAttribute("data-markerindex");

        // Close all other infoboxes
        for (var j = 0; j < this.markers.length; j++) {
            this.markers[j].infobox.close(this.map, this.markers[j]);
            this.markers[j].setIcon(iconNormal);
        }
        // Open correct info box
        this.markers[id].infobox.open(this.map, this.markers[id]);
        this.markers[id].setIcon(iconSelected);
    }
};
new MapAndProperties();